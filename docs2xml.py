import os, sys, subprocess, glob
from zipfile import *
from pyquery import PyQuery as pq
import settings
import getID
import content_types.chrestomathy
import content_types.biography
import content_types.interest
import content_types.dictionary
import content_types.table


def main():
	#convert()
	extract()
	parser()


def convert():
	os.chdir(settings.PATH_TO_SOURCES)
	os.mkdir('%stemp' % settings.OUTPUT_DIRECTORY)
	for content_type in settings.CONTENT_TYPES:
		os.chdir(settings.DIRECTORY_NAMES[content_type])
		os.mkdir('%stemp/%s' % (settings.OUTPUT_DIRECTORY, content_type))
		temp_dir = '%stemp/%s/' % (settings.OUTPUT_DIRECTORY, content_type)
		files = glob.glob('*.doc')
		for file in files:
			convert_process = subprocess.Popen('unoconv -v -o %s -f odt %s' % (temp_dir, file), shell=True,
											   stdout=subprocess.PIPE)
			out = convert_process.stdout.read()
			if settings.DEBUG:
				print(out)
		os.chdir(settings.PATH_TO_SOURCES)


def extract():
	os.chdir('%stemp/' % settings.OUTPUT_DIRECTORY)
	for content_type in settings.CONTENT_TYPES:
		os.chdir(content_type)
		for file in os.listdir():
			print('Extract %s to extracted/%s' % (file, file[:-4]))
			archive = ZipFile(file, 'r')
			archive.extractall('extracted/%s' % file[:-4])
		os.chdir('%stemp/' % settings.OUTPUT_DIRECTORY)


def parser():
	id_list = getID.getID(settings.ARTICLES_CONTENT)
	os.chdir('%stemp/' % settings.OUTPUT_DIRECTORY)
	num_in_cat = 1
	for content_type in settings.CONTENT_TYPES:
		print('Generating %s.xml...' % content_type)
		os.chdir('%s/extracted' % content_type)
		output_xml = pq('<root><paragraph version="0.76" type="hidden"></paragraph></root>')
		root_block = output_xml("paragraph").eq(0)
		for directory in os.listdir():
			os.chdir(directory)
			text = os.path.abspath('content.xml')
			if 'Pictures' in os.listdir():
				os.chdir('Pictures')
				try:
					os.mkdir('%s%s' % (settings.PATH_TO_IMAGES, content_type))
				except FileExistsError:
					r = 10
				image_dir = '%s%s' % (settings.PATH_TO_IMAGES, content_type)
				if len(os.listdir()) > 0:
					picture = os.listdir()[0]
					os.rename(picture, '%s/%s.jpg' % (image_dir, directory))
					image = '%s.jpg' % directory
				else:
					image = 'anonymous.jpg'
				os.chdir('..')
			else:
				image = 'anonymous.jpg'

			if content_type == 'chrestomathy':
				root_block.append("\n%s\n" % content_types.chrestomathy.main(text, id_list, settings.BOOK, num_in_cat))
			elif content_type == 'biography':
				root_block.append("\n%s\n" % content_types.biography.main(text, id_list, settings.BOOK, image, num_in_cat))
			elif content_type == 'interest':
				root_block.append("\n%s\n" % content_types.interest.main(text, id_list, settings.BOOK, image, num_in_cat))
			elif content_type == 'dictionary':
				root_block.append("\n%s\n" % content_types.dictionary.main(text, id_list, settings.BOOK, num_in_cat, image))
			elif content_type == 'table':
				root_block.append("\n%s\n" % content_types.table.main(text, id_list, settings.BOOK, num_in_cat))
			os.chdir("..")
			num_in_cat += 1
		os.chdir(settings.OUTPUT_DIRECTORY)

		output_file = open('%s.xml' % content_type, 'w')
		output_file.write(output_xml.html())
		output_file.close()
		print("[Ok]")
		os.chdir('%stemp/' % settings.OUTPUT_DIRECTORY)


if __name__ == '__main__':
	main()