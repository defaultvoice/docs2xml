#-*- coding: utf-8 -*-
from pyquery import PyQuery as pq

def main(file, num, book, num_in_cat):
	ODT = pq(filename=file)
	ODT.remove_namespaces()
	if book == 'biology':
		header = ODT('table-row').eq(0).text()
		author = '<p align="right"><bi>%s</bi></p>' % ODT('table-row').eq(2).text()
		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		i = 0
		xml = pq('\n\n\t<content_block id="%s" class="chrestomathy">\n\t\t<header>%s</header>\n\t\t<text_block>\n</text_block>\n\t</content_block>' % (id, header))

		text_block = xml('text_block').eq(0)
		while i < ODT('table-row').eq(1).find('p').length:
			paragraph = ODT('table-row').eq(1).find('p').eq(i).text()
			paragraph = paragraph.replace("<...>", "&#60;...&#62;")
			text_block.append('\t\t\t<p>%s</p>\n' % paragraph)
			i += 1

		text_block.append('\n\t\t%s' % author)
	elif book == 'history_rf':
		paragraphs = [i.text() for i in ODT.items('p')]
		i = 0
		while i < paragraphs.count(''):
			paragraphs.remove('')
		paragraphs.pop(0)
		header = paragraphs[0]
		author = paragraphs[-1]
		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		xml = pq('\t\t<content_block id="%s" class="chrestomathy">\n\t<header>%s</header>\n\t<text_block>\n</text_block>\n</content_block>' % (id, header))
		text_block = xml('text_block').eq(0)
		i = 1
		while i < len(paragraphs) - 1:
			paragraph = paragraphs[i]
			paragraph = paragraph.replace("<...>", "&#60;...&#62;")
			paragraph = paragraph.replace("<…>", "&#60;...&#62;")
			text_block.append('\t\t<p>%s</p>\n' % paragraph)
			i += 1
		text_block.append('\t\t<p><bi>%s</bi></p>\n\t' % author)

	return xml