#-*- coding: utf-8 -*-
from pyquery import PyQuery as pq

def main(file, num, book, num_in_cat):
	ODT = pq(filename=file)
	ODT.remove_namespaces()
	print("Hello, i'm table parser")
	if book == 'biology':
		rows = ODT('table-row')
		xml = pq('<table></table>')
		table = xml('table')
		i = 0
		while i < len(rows):
			row = rows.eq(i)
			if i == len(rows) - 1:
				table.append('\n\t\t\t<tr></tr>\n\t\t')
			else:
				table.append('\n\t\t\t<tr></tr>')
			xml_row = table('tr').eq(i)
			cols = row.find('table-cell')
			j = 0
			while j < len(cols):
				if j == len(cols) - 1:
					xml_row.append('\n\t\t\t\t<td></td>\n\t\t\t')
				else:
					xml_row.append('\n\t\t\t\t<td></td>')
				td = xml_row('td').eq(j)
				try:
					col = pq(cols.eq(j).text())
				except Exception:
					print("Houston, we've had a problem.\nFilename: %s" % file)
					break
				if i == 0:
					td.append('<b>%s</b>' % col.html())
				else:
					td.append('%s' % col.html())
				j += 1
			i += 1
		ODT('table').empty()
		header = ODT.text()
		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		work_xml = pq('\n\n\t<content_block id="%s" class="table">\n\t\t<header>%s</header>\n\t\t<text_block>\n\t\t%s\n\t</text_block>\n</content_block>' % (id, header, xml))
		print('All system go')
		return work_xml