#-*- coding: utf-8 -*-
from pyquery import PyQuery as pq

def main(file, num, book, num_in_cat, image):
	ODT = pq(filename=file)
	ODT.remove_namespaces()
	if book == 'history_rf':
		paragraphs = [i.text() for i in ODT.items('p')]
		paragraphs = [paragraph for paragraph in paragraphs if paragraph != '']
		if paragraphs[0] == 'Словарь':
			paragraphs.pop(0)
		header = paragraphs[0]
		paragraphs.pop(0)
		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		xml = pq('\t\t<content_block id="%s" class="dictionary">\n\t<header>%s</header>\n\t<text_block>\n</text_block>\n</content_block>' % (id, header))
		text_block = xml('text_block').eq(0)
		for paragraph in paragraphs:
			paragraph = paragraph.replace("<...>", "&#60;...&#62;")
			text_block.append('\t\t<p>%s</p>\n' % paragraph)
	elif book == 'biology':
		try:
			header = ODT('span').text()
		except IndexError:
			print('Oh, shit! Header does not exist\nFile:\n%s' % file)

		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		text = '\t\t<p>%s</p>\n' % ODT.text()
		xml = pq('\t\t<content_block id="%s" class="dictionary">\n\t<header>%s</header>\n\t<text_block>\n%s</text_block>\n</content_block>' % (id, header, text))
	elif book == 'chemistry':
		paragraphs = [i.text() for i in ODT.items('p')]
		paragraphs = [paragraph for paragraph in paragraphs if paragraph != '']
		if paragraphs[0] == 'Определение':
			paragraphs.pop(0)
		header = paragraphs[0]
		paragraphs.pop(0)
		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat
		xml = pq('\t\t<content_block id="%s" class="dictionary">\n\t<header>%s</header>\n\t<text_block>\n</text_block>\n</content_block>' % (id, header))
		text_block = xml('text_block').eq(0)
		for paragraph in paragraphs:
			paragraph = paragraph.replace("<...>", "&#60;...&#62;")
			text_block.append('\t\t<p>%s</p>\n' % paragraph)
		if image != 'anonymous.jpg':
			text_block.append('\t\t<image_block class="full-width">\n\t\t\t<img url="Data\\images\\dictionary\\%s"/>\n\t\t</image_block>\n' % image)
	return xml