#-*- coding: utf-8 -*-
from pyquery import PyQuery as pq


def main(file, num, book, image, num_in_cat):
	ODT = pq(filename=file)
	ODT.remove_namespaces()

	if book == 'history_rf':
		paragraphs = ODT('p')
		i = 0
		text = []
		while i < paragraphs.length:
			paragraph = paragraphs.eq(i)
			paragraph('table-row').find('p').empty()
			if paragraph.text() != '':
				text.append(paragraph.text())
			i += 1

		ODT = pq(filename=file)
		ODT.remove_namespaces()

		i = 0
		rows = ODT('table-row')
		while i < rows.length:
			row = rows.eq(i)
			if row.text() != '':
				description = row.text()
			i += 1
		try:
			header = text[1]

			try:
				id = num[header]
			except KeyError:
				c_keys = [c for c in num.keys()]
				for key in c_keys:
					if header in key:
						id = num[key]
						break
				else:
					id = 'not_found_%d' % num_in_cat

			header += " %s" % text[2]
			description = header
			short = text[1]
			try:
				xml = pq('\t\t<content_block id="%s" class="biography">\n\t<header short="%s">%s</header>\n\t<text_block>\n\t\t<image_block class="inner">\n\t\t\t<img url="Data\\images\\biography\\%s"/>\n\t\t\t<p>%s</p>\n\t\t</image_block>\n</text_block>\n</content_block>' % (id, short, header, image, description))
			except UnboundLocalError:
				xml = pq('\t\t<content_block id="%s" class="biography">\n\t<header short="%s">%s</header>\n\t<text_block>\n\t\t<image_block class="inner">\n\t\t\t<img url="Data\\images\\biography\\%s"/>\n\t\t\t<p></p>\n\t\t</image_block>\n</text_block>\n</content_block>' % (id, short, header, image))
			text_block = xml('text_block').eq(0)
			i = 3
			while i < len(text):
				paragraph = text[i]
				paragraph = paragraph.replace("<...>", "&#60;...&#62;")
				paragraph = paragraph.replace("<…>", "&#60;...&#62;")
				if i == len(text) - 1:
					text_block.append('\t\t<p>%s</p>\n\t' % paragraph)
				else:
					text_block.append('\t\t<p>%s</p>\n' % paragraph)
				i += 1
			return xml
		except IndexError:
			print("Warning! Text is not exist! Info: %s" % text)

	elif book == 'biology':
		rows = ODT('table-row')
		header = rows.eq(0).text()
		paragraphs = rows.eq(1).find('p')
		i = 0
		text = []

		while i < paragraphs.length:
			paragraph = paragraphs.eq(i)
			paragraph('table-row').find('p').empty()
			if paragraph.text() != '':
				text.append(paragraph.text())
			i += 1

		description = header

		try:
			id = num[header]
		except KeyError:
			c_keys = [c for c in num.keys()]
			for key in c_keys:
				if header in key:
					id = num[key]
					break
			else:
				id = 'not_found_%d' % num_in_cat

		try:
			xml = pq('\t\t<content_block id="%s" class="biography">\n\t<header>%s</header>\n\t<text_block>\n\t\t<image_block>\n\t\t\t<img url="Data\\images\\biography\\%s"/>\n\t\t\t<p>%s</p>\n\t\t</image_block>\n</text_block>\n</content_block>' % (id, header, image, description))
		except UnboundLocalError:
			xml = pq('\t\t<content_block id="%s" class="biography">\n\t<header>%s</header>\n\t<text_block>\n\t\t<image_block>\n\t\t\t<img url="Data\\images\\biography\\%s"/>\n\t\t\t<p></p>\n\t\t</image_block>\n</text_block>\n</content_block>' % (id, header, image))
		text_block = xml('text_block').eq(0)
		text_block.append('\t\t<p>%s</p>\n\t' % rows.eq(2).text())
		i = 0
		while i < len(text):
			paragraph = text[i]
			paragraph = paragraph.replace("<...>", "&#60;...&#62;")
			if i == len(text) - 1:
				text_block.append('\t\t<p>%s</p>\n\t' % paragraph)
			else:
				text_block.append('\t\t<p>%s</p>\n' % paragraph)
			i += 1

		return xml