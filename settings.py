#Docs2Xml settings file

DEBUG = False
CONVERTER = 'unoconv'
PATH_TO_SOURCES = '/home/defaultvoice/Desktop/data2/'
PATH_TO_IMAGES = '/home/defaultvoice/Desktop/images/'
OUTPUT_DIRECTORY = '/home/defaultvoice/Desktop/paragraphs/'
ARTICLES_CONTENT = '/home/defaultvoice/Desktop/articles_content.xml'
BOOK = 'history_rf'
CONTENT_TYPES = [
#	'chrestomathy',
	'biography',
#	'interest',
#	'dictionary',
#	'table'
]

DIRECTORY_NAMES = {
	'chrestomathy': 'xrestomatia',
	'biography': 'biography',
	'interest': 'interest',
	'dictionary': 'vocab',
	'table': 'table'
}