#-*- coding: utf-8 -*-
from pyquery import PyQuery as pq


def main(colnum, article):
	xml = pq(article)
	paragraphs = xml.find('p')
	paragraphs = [p.text() for p in paragraphs.items()]
	print(paragraphs)
	xml = pq('<table></table>')
	table = xml('table')
	i = 0
	j = 0
	rownum = len(paragraphs)//colnum
	while i < rownum:
		row = pq('<tr></tr>')
		while j < colnum:
			col = pq('<td>%s</td>' % paragraphs.pop(0))
			if j == colnum - 1:
				row.append('\n\t\t%s\n\t' % col)
			else:
				row.append('\n\t\t%s' % col)
			j += 1
		j = 0
		if i == rownum - 1:
			table.append('\n\t%s\n' % row)
		else:
			table.append('\n\t%s' % row)
		i += 1
	print(xml)
main(2, '''<p>Виды загрязнений</p>
		<p>Отрасль промышленности, для которой характерен данный вид загрязнений</p>
		<p>CO 2 , SO 2 , NO, NO 2</p>
		<p>Тепловая энергетика, транспорт, металлургическая промышленность, переработка руд, содержащих серу</p>
		<p>Галогеносодержащие соединения</p>
		<p>Химическая, холодильная промышленность</p>
		<p>Металлические частицы</p>
		<p>Металлургическая, горнодобывающая промышленность</p>
		<p>Углеводороды</p>
		<p>Тепловая энергетика, транспорт</p>''')
