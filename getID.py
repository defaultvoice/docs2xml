# -*- coding: utf-8 -*-
from pyquery import PyQuery as pq


def getID(articles_content):
	xml = pq(filename=articles_content)
	dict_id = {}
	articles_list = xml("article")
	i = 0
	while i < len(articles_list):
		article = articles_list.eq(i)
		id = article.find('id').text()
		i += 1
		header = article('table').find('h2').eq(0).text()
		# if 'interest' in id:
		# 	dict_id[header] = id
		# elif 'dictionary' in id:
		# 	dict_id[header] = id
		if 'biography' in id:
			dict_id[header] = id
		#if 'xrestomatia' in id:
		#	dict_id[header] = id
		# elif 'table' in id:
		# 	dict_id[header] = id
		#if 'reference' in id:
		#	dict_id[header] = id
	return dict_id


if __name__ == '__main__':
	getID()